const { GoogleSpreadsheet } = require('google-spreadsheet');
let serviceAccount = require(process.env.GOOGLE_APPLICATION_CREDENTIALS)

let sheetId = process.env.GOOGLE_SHEET_ID || "1hodVmzhNU88IFjKeQc0-avYSWtmXpe0m6N_bBsv-Y98"
const doc = new GoogleSpreadsheet(sheetId);

async function getSheetObject(action=null){
    let sheet = null
    await doc.useServiceAccountAuth({
        client_email: serviceAccount.client_email,
        private_key: serviceAccount.private_key,
    });
    
    await doc.loadInfo();
    if(!action){
        action = process.env.ACTION
    }

    if (action == "produce") {
        sheet = doc.sheetsByIndex[0]; // or use doc.sheetsById[id] or doc.sheetsByTitle[title]
    } else {
        sheet = doc.sheetsByIndex[1]; // or use doc.sheetsById[id] or doc.sheetsByTitle[title]
    }
    return sheet
}

module.exports = {
    getSheetObject,
}

