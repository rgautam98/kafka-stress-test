let brokers = process.env.KAFKA_BROKERS.split(",")

let config = {
    topic: process.env.KAFKA_TOPIC || "example-topic",
    analyticsTopic: process.env.ANALYTICS_TOPIC || "analytics",
    chunksTopic: process.env.CHUNKS_TOPIC || "chunks",
    brokers,
    // username: "username",
    // password: "password",
    // authMechanism: "plain", // plain, scram-sha-256 or scram-sha-512
    ssl: false 
}

module.exports = config
