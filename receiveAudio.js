const { Kafka } = require('kafkajs')
var fs = require('fs');
let config = require('./config')
const { generateId } = require('./helpers');
let { upload } = require('./s3');
// const { getSheetObject } = require('./gsheets');

let topic = config.topic
let fromBeginning = process.env.FROM_BEGINNING ? true : false
/*
 If this is true and autocommit is set to true, then you will read all the messages only once. 
 autoCommit = true means to read from beginning, resetOffsets should be true
*/
let resetOffsets = false;

let output_file = 'stream-output.mp3'
let consumerId = ''
let sheet = null
let consumerSessionTimeout = parseInt(process.env.CONSUMER_SESSION_TIMEOUT) || 60000 // in milliseconds

const kafka = new Kafka({
    clientId: 'audio-consumer',
    brokers: config.brokers,
    authenticationTimeout: 45000,
    ssl: config.ssl,
    // sasl: {
    //     mechanism: config.authMechanism, // plain or scram-sha-256 or scram-sha-512
    //     username: config.username,
    //     password: config.password
    // },
})

const analyticsProducer = kafka.producer()


// Application specific varaiables
let audio_chunks = {}
let audio_log_metadata = {}

async function handleAudioChunks(message){
    if(message.start){
        console.log("Started recording audio for audioId: " + message.audioId)
        audio_chunks[message.audioId] = []
        audio_log_metadata[message.audioId] = {
            start: new Date().getTime()
        }
        console.log(audio_chunks, audio_log_metadata)
    } else if (message.progress){
        // Save the chunks to audio_chunks[message.audioId]
        if(!audio_chunks[message.audioId]){
            console.log("Started recording audio for " + message.audioId )
            audio_chunks[message.audioId] = []
            audio_log_metadata[message.audioId] = {
                start: new Date().getTime()
            }
        }
        audio_chunks[message.audioId].push(message.index)
        // {
        //     index: message.index,
        //     audio_data_chunk: message.audio_data_chunk,
        //     timestamp: message.timestamp
        // })

        if(process.env.ENABLE_VERBOSE_LOGGING){
            console.log("Saved chunk: " + message.index + " ")
        }

        let timestamp = new Date().getTime()
        let analyticsObject = {
            "ConsumerId": consumerId + '-chunk',
            "AudioId": message.audioId + '-chunk',
            "chunks": message.index,
            "TotalSize": message.audio_data_chunk.data.length,
            "start": message.timestamp,
            "end" : timestamp,
            "difference": timestamp - message.timestamp,
            "StoragePath": `none`,
            "source": "consume",
            "loggedToSheet": "false"
        }

        if(process.env.PUBLISH_CHUNK_ANALYTICS){
            
            await analyticsProducer.send({
                topic: config.chunksTopic,
                messages: [
                    {
                        key: consumerId, value: JSON.stringify(analyticsObject)
                    },
                ],
                timeout: 300000
            })
        }




    } else if (message.end){
        console.log("Ended recording audio for " + message.audioId)
        audio_log_metadata[message.audioId].end = new Date().getTime()
        // sort chunks by index
        // audio_chunks[message.audioId].sort((a, b) => a.index - b.index)

        // let file_upload_path = message.audioId + "-" + output_file
        let file_upload_path = `${message.audioId}-${consumerId}-${output_file}`
        // save the chunks to a file
        // for(let i of audio_chunks[message.audioId]){
        //     fs.appendFileSync(file_upload_path, Buffer.from(i.audio_data_chunk), {
        //         encoding: 'binary'
        //     })
        // }
        console.log("Saved audio chunks to file: " + file_upload_path)

        if(process.env.ALLOW_UPLOAD_TO_S3){
            // upload the file to s3
            await upload(`${consumerId}/${message.audioId}.mp3`, fs.readFileSync(file_upload_path))
        }

        let file_size = '0'

        let analyticsObject = {
            "ConsumerId": consumerId,
            "AudioId": message.audioId,
            "chunks": audio_chunks[message.audioId].length,
            "TotalSize": file_size,
            "start": audio_log_metadata[message.audioId].start,
            "end": audio_log_metadata[message.audioId].end,
            "difference": audio_log_metadata[message.audioId].end - audio_log_metadata[message.audioId].start,
            "StoragePath": `https://storage.googleapis.com/bucket/${consumerId}/${message.audioId}.mp3`,
            "source": "consume",
            "finalObject": true
        }

        let loggedToSheet = false

        try {
            // await sheet.addRow(analyticsObject, {
            //     insert: true
            // })
            loggedToSheet = false
        } catch(err) {
            console.log("Error Logging to Sheet")
            console.log(err)
        }

        analyticsObject.loggedToSheet = loggedToSheet

        await analyticsProducer.send({
            topic: config.analyticsTopic,
            messages: [
                {
                    key: consumerId, value: JSON.stringify(analyticsObject)
                },
            ],
            timeout: 300000
        })

        console.log("Logged analytics data")
        audio_chunks[message.audioId] = null
        audio_log_metadata[message.audioId] = null
    }
}

async function main(){
    consumerId = await generateId()
    // sheet = await getSheetObject()
    await analyticsProducer.connect()

    let groupId = consumerId + '-consumer'

    if(process.env.USE_SAME_CONSUMER_GROUP){
        groupId = 'audio-processing-consumer'
        consumerId = consumerId + '-audio-processing-consumer'
    }

    if (process.env.FROM_BEGINNING) {
        groupId = groupId + '-from-beginning'
    }

    const consumer = kafka.consumer({ 
        groupId: groupId,
        heartbeatInterval: consumerSessionTimeout/2,
        sessionTimeout: consumerSessionTimeout,
     })
    await consumer.connect()
    console.log("Connected to Kafka with the consumer id: " + consumerId)
    await consumer.subscribe({ topics: [topic], fromBeginning })
    console.log("Subscribed to topic")

    await consumer.run({
        autoCommit: true,
        eachMessage: async (msgObj) => {
            // console.log("The message object is")
            // console.log(msgObj)
            // there are heartbeat and pause functions in msgObj
            // console.log("The message is")
            // console.log({
            //     partition: msgObj.partition,
            //     key: msgObj.message.key ? msgObj.message.key.toString(): "",
            //     value: msgObj.message.value.toString(),
            //     offset: msgObj.message.offset,
            //     headers: msgObj.message.headers,
            // })
            await msgObj.heartbeat()
            // If your process is taking time. Else the message will be delivered to another consumer
            handleAudioChunks(JSON.parse(msgObj.message.value.toString()))
        },
    })

    if(resetOffsets){
        consumer.commitOffsets([
            { topic: topic, partition: 0, offset: '0' },
        ])
    }
}

if (require.main === module) {
    main();
}

module.exports = main
