const { Kafka } = require('kafkajs')
let config = require('./config')
// const { getSheetObject } = require('./gsheets');
let fs = require('fs')
// const process = require('process');

let topic = config.analyticsTopic
let fromBeginning = false
let autoCommit = true
let resetOffsets = false;

let consumerId = ''
// let producerSheet = null
// let consumerSheet = null

let producerFile = 'producer.csv'
let consumerFile = 'consumer.csv'

let total_chunk_count = 0
let avg_time_diff = 0
let max_time_taken = 0
let min_time_taken = Infinity

let consumerSessionTimeout = parseInt(process.env.CONSUMER_SESSION_TIMEOUT) || 60000 // in milliseconds

const kafka = new Kafka({
    clientId: 'analytics-worker-all',
    brokers: config.brokers,
    authenticationTimeout: 450000,
    ssl: config.ssl,
    // sasl: {
    //     mechanism: config.authMechanism, // plain or scram-sha-256 or scram-sha-512
    //     username: config.username,
    //     password: config.password
    // },
})

async function handleAnalyticsMessage(message){

    
    if (message.source == 'consume' && message.StoragePath == 'none') {
        total_chunk_count += 1
        avg_time_diff = (parseInt(message.difference)+((total_chunk_count-1) * avg_time_diff))/total_chunk_count
    
        if(parseInt(message.difference) > max_time_taken){
            max_time_taken = parseInt(message.difference)
        }
    
        if(parseInt(message.difference) < min_time_taken){
            min_time_taken = parseInt(message.difference)
        }

        console.log(`${message.difference} :::: ${avg_time_diff} : ${max_time_taken} : ${min_time_taken}`)

        fs.appendFileSync(consumerFile, [
            message.ConsumerId,
            message.AudioId,
            message.chunks,
            message.TotalSize,
            message.start,
            message.end,
            message.difference,
            message.StoragePath
        ].join(", ") + "\n")
        console.log(`total_chunk_count: ${total_chunk_count}`)
        console.log(`avg_time_diff: ${avg_time_diff}`)
        console.log(`max_time_taken: ${max_time_taken}`)
        console.log(`min_time_taken: ${min_time_taken}`)

    } else {

        console.log(Object.values(message).join(", "))
        if(message.source == 'consume'){
            fs.appendFileSync(consumerFile, [
                message.ConsumerId,
                message.AudioId,
                message.chunks,
                message.TotalSize,
                message.start,
                message.end,
                message.difference
            ].join(", ") + "\n")

        } else {
            // producer
            fs.appendFileSync(producerFile, [
                message.AudioId,
                message.chunks,
                message.TotalSize,
                message.start,
                message.end,
                message.difference
            ].join(", ") + "\n")
        }

    }



}

async function main(){
    consumerId = 'analytics-all'
    // producerSheet = await getSheetObject('produce')
    // consumerSheet = await getSheetObject('consume')


    fs.writeFileSync(producerFile, 'AudioId, chunks, TotalSize, start, end, difference\n')
    fs.writeFileSync(consumerFile, 'ConsumerId, AudioId, chunks, TotalSize, start, end, difference\n')
    const consumer = kafka.consumer({ 
        groupId: consumerId + '-consumer',
        heartbeatInterval: consumerSessionTimeout/2,
        sessionTimeout: consumerSessionTimeout,
     })
    await consumer.connect()
    console.log("Connected to Kafka with the consumer id: " + consumerId)
    await consumer.subscribe({ topics: [topic], fromBeginning })
    console.log("Subscribed to topic")
    console.log("autoCommit: ", autoCommit)

    await consumer.run({
        autoCommit: autoCommit,
        eachMessage: async (msgObj) => {
            // console.log("The message object is")
            // console.log(msgObj)
            // there are heartbeat and pause functions in msgObj
            // console.log("The message is")
            // console.log({
            //     partition: msgObj.partition,
            //     key: msgObj.message.key ? msgObj.message.key.toString(): "",
            //     value: msgObj.message.value.toString(),
            //     offset: msgObj.message.offset,
            //     headers: msgObj.message.headers,
            // })
            await msgObj.heartbeat()
            // If your process is taking time. Else the message will be delivered to another consumer
            handleAnalyticsMessage(JSON.parse(msgObj.message.value.toString()))
        },
    })

    if(resetOffsets){
        consumer.commitOffsets([
            { topic: topic, partition: 0, offset: '0' },
        ])
    }
}

if (require.main === module) {
    main();
}

module.exports = main
