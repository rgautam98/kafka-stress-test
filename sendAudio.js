const { Kafka } = require('kafkajs')
let path = require('path');
var fs = require('fs');
let config = require('./config')
const { generateId } = require('./helpers');
// const { getSheetObject } = require('./gsheets');

let audio_path = path.resolve(process.cwd(), 'audio.mp3')

let index = 0;

let topic = config.topic
let sheet = null

const kafka = new Kafka({
    clientId: 'audio-producer',
    brokers: config.brokers,
    authenticationTimeout: 45000,
    ssl: config.ssl,
    // sasl: {
    //     mechanism: config.authMechanism, // scram-sha-256 or scram-sha-512
    //     username: config.username,
    //     password: config.password
    // },

})

function waitForInputBeforeTerminate(){
    console.log("Press Ctrl+C to terminate")
    while(true){}
}

async function main() {
    let audioId = await generateId()
    // sheet = await getSheetObject()
    const producer = kafka.producer()

    await producer.connect()
    console.log("Connected with the audio id ", audioId)
    console.log("Sending message to Kafka topic ", topic)

    var audio_data = fs.readFileSync(audio_path);
    const CHUNK_SIZE = parseInt(process.env.CHUNK_SIZE) || 1000; // 1 KB

    // Inform of start of sending to consumer
    let start = new Date().getTime()
    await producer.send({
        topic,
        messages: [
            {
                key: audioId, value: JSON.stringify({
                    audioId: audioId,
                    start: true
                })
            },
        ],
    })

    for (let bytesRead = 0; bytesRead < audio_data.length; bytesRead = bytesRead + CHUNK_SIZE) {

        if (process.env.ENABLE_VERBOSE_LOGGING) {
            console.log(audioId + " : " + index + " chunk sent")
        }

        let audio_data_chunk = audio_data.slice(bytesRead, bytesRead + CHUNK_SIZE)

        await producer.send({
            topic,
            messages: [
                {
                    key: audioId, value: JSON.stringify({
                        index,
                        audio_data_chunk: audio_data_chunk,
                        timestamp: new Date().getTime(),
                        audioId: audioId,
                        progress: true
                    })
                },
            ],
        })
        index++
    }

    // inform of end so that processing starts
    await producer.send({
        topic,
        messages: [
            {
                key: audioId, value: JSON.stringify({
                    audioId: audioId,
                    chunk: index,
                    end: true
                })
            },
        ],
    })
    let end = new Date().getTime()


    let file_size = fs.statSync(audio_path).size

    let analyticsObject = {
        "AudioId": audioId,
        "chunks": index,
        "TotalSize": file_size,
        "start": start,
        "end": end,
        "difference": end - start,
        "loggedToSheet": false,
        "source": "produce"
    }

    try {
        // await sheet.addRow(analyticsObject, {
        //     insert: true
        // })
        analyticsObject.loggedToSheet = false
    } catch(err){
        console.log("Error in logging analytics data to Google Sheets")
        console.log(err)
    }
    // log to kafka

    await producer.send({
        topic: config.analyticsTopic,
        messages: [
            {
                key: audioId, value: JSON.stringify(analyticsObject)
            },
        ]
    })

    console.log("Logged analytics data")

    await producer.disconnect()
    console.log("Disconnected from Kafka")
    waitForInputBeforeTerminate()

}

if(require.main === module){
    main();
}

module.exports = main
