FROM node:16-alpine

WORKDIR /app

RUN mkdir -p /app
COPY package.json /app

RUN npm install
COPY . /app

CMD ["npm", "start"]
