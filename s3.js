const AWS = require('aws-sdk')

const config = {
    endpoint: process.env.AWS_ENDPOINT,
    bucketName: process.env.AWS_BUCKET_NAME,
    region: process.env.AWS_REGION,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    httpOptions: { timeout: 0 },
    maxRetries: 10,
    correctClockSkew: true,
}

const s3 = new AWS.S3(config)

const upload = async (key, body) => {

    const params = {
        Bucket: config.bucketName,
        Key: key,
        Body: body
    }

    console.log("Uploading to ", key)

    // s3.upload(params, (s3Err, s3data) => {
    //     if (s3Err) {
    //         console.log('S3: Error ', s3Err)
    //     } else {
    //         console.log("Uploaded")
    //     }
    // })

    let uploaded = await s3.upload(params).promise()
    console.log(uploaded)
    return true
}


// upload("data.txt", "This is some random data")

module.exports = {
    upload
}