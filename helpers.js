let nanoid = require("nanoid/async")

const idGenerator = nanoid.customAlphabet('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 10)


async function generateId(){
    return await idGenerator()
}

module.exports = {
    generateId
}