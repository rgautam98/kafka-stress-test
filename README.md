# Using kafka to send audio files

The repository has the necessary code to send and receive audio files using kafka. 

To run this code in your local system, please follow the steps below. 

## Prerequisites

You will need to have a working kafka cluster. You can spin one in your local system using the docker-compose in https://ksqldb.io/quickstart.html. 

Alternatively, you can spin up a development cluster in https://confluent.cloud/ or https://www.cloudkarafka.com/. 

Both the platforms have a generous free tier and confluent cloud has managed ksqldb as a service as well. 

I personally prefer confluent cloud. 

## Steps

After the kafka cluster is ready, head over to `config.js` and set the topic name and the cluster credentials. 

Post that, run the following commands. 

- `npm install`
- `npm start receive` -> This will start the consumer to receive the audio files
- `npm start send` -> This will start the producer to send the audio files. 

By default, it will read the test-audio.mp3 located in the project root. 

The final output is saved in stream-output.mp3 in the project root.


