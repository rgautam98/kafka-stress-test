let axios = require("axios")

function delay(time) {
    return new Promise(resolve => setTimeout(resolve, time));
} 

let token = process.env.SERVICE_TOKEN
let testingEndpoint = process.env.TESTING_ENDPOINT

async function doRequest(url, acceptedStatus) {

    let response = null
    let err = null
    try {
        response = await axios.request(config = {
            method: 'put',
            maxBodyLength: Infinity,
            url: testingEndpoint,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            data: JSON.stringify({
                "id": "random",
                "email": "gautam@fireflies.ai"
            })
        })
        console.log(`SUCCESS: ${url}`)

        
    } catch (e) {
        err = e
        console.log(`FAIL: ${url}`)
        console.log(err)
    }
}

async function main() {
    console.log("Starting Axios Tests")
    let i = 0;

    while(i < 10){
        await doRequest('https://testing', 200)
        await doRequest('https://testing', 200)
        await delay(10000);
        i++
    }
    console.log("Ending Axios Tests")
    await delay(5000);
}

if(require.main == module){
    main()
}
